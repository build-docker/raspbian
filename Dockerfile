FROM registry.icinga.com/build-docker/raspbian-base/bullseye

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get upgrade -y \
 && rm -rf /var/lib/apt/lists/* # 20190516

RUN apt-get update \
 && apt-get install -y \
  sudo wget curl tar expect git \
  devscripts build-essential lintian ccache pbuilder aptitude apt-utils \
 && rm -rf /usr/share/doc/* /usr/share/locales/* /usr/share/man/* \
 && rm -rf /var/lib/apt/lists/*

RUN curl -k https://packages.icinga.com/icinga.key | apt-key add - \
 && DIST=$(awk -F"[)(]+" '/VERSION=/ {print $2}' /etc/os-release) \
 && DIST=bullseye \
 && echo "deb http://packages.icinga.com/raspbian icinga-${DIST} main" > /etc/apt/sources.list.d/${DIST}-icinga.list

# Install build deps for Icinga 2 and Icinga Web 2
RUN apt-get update \
 && apt-get install -y \
    bash-completion debhelper make po-debconf \
    bison cmake pkg-config libsystemd-dev flex g++ libboost-dev libboost-program-options-dev libboost-regex-dev libboost-system-dev libboost-test-dev libboost-thread-dev default-libmysqlclient-dev libpq-dev libssl-dev libyajl-dev libedit-dev \
    php-cli php-htmlpurifier node-uglify \
 && rm -rf /var/lib/apt/lists/*

RUN groupadd -g 1000 build \
 && useradd -u 1000 -g 1000 -m build \
 && echo 'Defaults:build !requiretty' | tee -a /etc/sudoers \
 && echo 'build ALL=(ALL:ALL) NOPASSWD: ALL' | tee -a /etc/sudoers \
 && chown build.build /usr/local/bin

RUN git clone https://git.icinga.com/build-docker/scripts.git /usr/local/bin
RUN icinga-provide-go armv6l
USER build
ENTRYPOINT ["/usr/local/bin/icinga-build-entrypoint"]
CMD ["icinga-build-package"]
